(function ($, Drupal) {

  Drupal.behaviors.sinpar_theme = {
    attach: function(context, settings) {
      // Get your Yeti started.
      //Calling caroufredsel
      
      		//recent projects carousel
	       $(".recent_projects_carousel ul").carouFredSel({
				circular: false,
				infinite: true,
				auto: false,
				scroll:{items:1},
				prev: { button: "#slide_prev", key: "left"},
				next: { button: "#slide_next",key: "right"}
			});
			
			$(".testimonial_slide").carouFredSel({
				circular: false,
				infinite: true,
				auto: false,
				scroll:{items:1},
				prev: { button: "#slide_prev1", key: "left"},
				next: { button: "#slide_next1",key: "right"}
			});

      
      
    }
  };

})(jQuery, Drupal);
