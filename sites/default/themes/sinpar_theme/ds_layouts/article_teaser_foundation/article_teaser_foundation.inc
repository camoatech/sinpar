<?php

/**
 * @file
 * Display Suite Article Teaser Foundation configuration.
 */

function ds_article_teaser_foundation() {
  return array(
    'label' => t('Article Teaser Foundation'),
    'regions' => array(
      'left' => t('left'),
      'right' => t('right'),
    ),
    // Uncomment if you want to include a CSS file for this layout (article_teaser_foundation.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (article_teaser_foundation.png)
    // 'image' => TRUE,
  );
}
