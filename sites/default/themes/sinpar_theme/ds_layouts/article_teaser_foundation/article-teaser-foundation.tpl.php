<?php
/**
 * @file
 * Display Suite Article Teaser Foundation template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $left: Rendered content for the "left" region.
 * - $left_classes: String of classes that can be used to style the "left" region.
 *
 * - $right: Rendered content for the "right" region.
 * - $right_classes: String of classes that can be used to style the "right" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> row clearfix">

  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <<?php print $left_wrapper; ?> class="large-6 columns left<?php print $left_classes; ?>">
      <?php print $left; ?>


    <?php if ($display_submitted): ?>
      <ul class="meta">
        <li><i class="foundicon-star"></i> by <?php print $name; ?></li>
        <li><i class="foundicon-calendar"></i> <?php print format_date($node->created, 'custom', 'M d, Y'); ?></li>
        <li><i class="foundicon-inbox"></i> <a href="<?php print $node_url;?>/#comments"><?php print $comment_count; ?> comments</a></li>

      </ul>
      <?php if (render($content['field_tags'])): ?>
        <div class="tags"><i class="icon-tags"></i><?php print render($content['field_tags']); ?></div>
      <?php endif; ?>

    <?php endif; ?>

    </<?php print $left_wrapper; ?>>

    <<?php print $right_wrapper; ?> class="large-6 columns ds-right<?php print $right_classes; ?>">
      <?php print $right; ?>
    </<?php print $right_wrapper; ?>>

</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
