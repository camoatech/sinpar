<?php

/**
 * @file
 * Display Suite Article Full Foundation configuration.
 */

function ds_article_full_foundation() {
  return array(
    'label' => t('Article Full Foundation'),
    'regions' => array(
      'top' => t('top'),
      'bottom' => t('bottom'),
    ),
    // Uncomment if you want to include a CSS file for this layout (article_full_foundation.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (article_full_foundation.png)
    // 'image' => TRUE,
  );
}
