<?php

/**
 * @file
 * Display Suite Portfolio Layout Foundation configuration.
 */

function ds_portfolio_layout_foundation() {
  return array(
    'label' => t('Portfolio Layout Foundation'),
    'regions' => array(
      'right' => t('right'),
      'left' => t('left'),
      'topleft' => t('top-left'),
      'bottomleft' => t('bottom-left'),
    ),
    // Uncomment if you want to include a CSS file for this layout (portfolio_layout_foundation.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (portfolio_layout_foundation.png)
    // 'image' => TRUE,
  );
}
