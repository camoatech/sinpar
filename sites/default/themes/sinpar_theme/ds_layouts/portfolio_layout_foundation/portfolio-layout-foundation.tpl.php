<?php
/**
 * @file
 * Display Suite Portfolio Layout Foundation template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $right: Rendered content for the "right" region.
 * - $right_classes: String of classes that can be used to style the "right" region.
 *
 * - $left: Rendered content for the "left" region.
 * - $left_classes: String of classes that can be used to style the "left" region.
 *
 * - $topleft: Rendered content for the "top-left" region.
 * - $topleft_classes: String of classes that can be used to style the "top-left" region.
 *
 * - $bottomleft: Rendered content for the "bottom-left" region.
 * - $bottomleft_classes: String of classes that can be used to style the "bottom-left" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="row<?php print $classes;?> clearfix">

  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <<?php print $right_wrapper; ?> class="large-8 columns <?php print $right_classes; ?>">
      <?php print $right; ?>
    </<?php print $right_wrapper; ?>>

    <<?php print $left_wrapper; ?> class="large-4 columns<?php print $left_classes; ?>">

	<div class="row">

		<<?php print $topleft_wrapper; ?> class="large-12 columns <?php print $topleft_classes; ?>">
		  <?php print $topleft; ?>
		</<?php print $topleft_wrapper; ?>>

		<<?php print $bottomleft_wrapper; ?> class="large-12 columns <?php print $bottomleft_classes; ?>">
		  <?php print $bottomleft; ?>
		</<?php print $bottomleft_wrapper; ?>>

     </div>  
		  <?php print $left; ?>

    </<?php print $left_wrapper; ?>>

</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
