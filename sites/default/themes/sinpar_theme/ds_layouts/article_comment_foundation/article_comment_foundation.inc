<?php

/**
 * @file
 * Display Suite Article Comment Foundation configuration.
 */

function ds_article_comment_foundation() {
  return array(
    'label' => t('Article Comment Foundation'),
    'regions' => array(
      'left' => t('left'),
      'topright' => t('top-right'),
      'bottomright' => t('bottom-right'),
    ),
    // Uncomment if you want to include a CSS file for this layout (article_comment_foundation.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (article_comment_foundation.png)
    // 'image' => TRUE,
  );
}
