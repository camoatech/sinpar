<?php
/**
 * @file
 * Display Suite Article Comment Foundation template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $left: Rendered content for the "left" region.
 * - $left_classes: String of classes that can be used to style the "left" region.
 *
 * - $topright: Rendered content for the "top-right" region.
 * - $topright_classes: String of classes that can be used to style the "top-right" region.
 *
 * - $bottomright: Rendered content for the "bottom-right" region.
 * - $bottomright_classes: String of classes that can be used to style the "bottom-right" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> row com_main clearfix">

  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <<?php print $left_wrapper; ?> class="ds-left small-2 columns avatar <?php print $left_classes; ?>">
      <?php print $left; ?>
    </<?php print $left_wrapper; ?>>

	<div class="small-10 columns right com_content">
		<<?php print $topright_wrapper; ?> class="ds-top-right large-12 columns com_title<?php print $topright_classes; ?>">
		  <?php print $topright; ?>
		</<?php print $topright_wrapper; ?>>

		<<?php print $bottomright_wrapper; ?> class="ds-bottom-right large-12 columns com_detail <?php print $bottomright_classes; ?>">
		  <?php print $bottomright; ?>
		</<?php print $bottomright_wrapper; ?>>
	</div>

</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
