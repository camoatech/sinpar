<?php
/**
 * @file field--media-gallery-field--media-gallery.tpl.php
 *
 * Wrap each image in a wrapper div to be used instead of the field-items class
 * both in the CSS and the JS.
 * This will solve problems with modules like fences or themes like zurb-foundation
 * or anything that takes away the default field-items class.
 *
 * @see http://developers.whatwg.org/grouping-content.html#the-div-element
 */
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php if (!$label_hidden): ?>
    <div class="media-gallery-file-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
  <?php endif; ?>
    <div class="media-gallery-file-items">
    <?php foreach ($items as $delta => $item): ?>
      <div class="media-gallery-file-item" id="media-gallery-media-<?php print $delta; ?>">
        <?php print render($item); ?></div>
    <?php endforeach; ?>

  </div>

</div>
